import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import PlaylistPage from './components/PlaylistPage';
import PlaylistDetails from './components/PlaylistDetails';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native'

const Stack = createStackNavigator();
function HomeScreen({navigation}) {
  return (
    <View style={styles.container}>
      <PlaylistPage navigation={navigation}/>
      <StatusBar backgroundColor="#fff" />
    </View>

  )
}
function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen 
          name="Editor's picks"
          component={HomeScreen}
          options={{
            headerStyle: { backgroundColor: "#000" },
            headerTintColor: "#fff" 
          }}
        />
        <Stack.Screen
          name="PlaylistDetails"
          component={PlaylistDetails}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App
const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    backgroundColor: '#000',
  },
});
