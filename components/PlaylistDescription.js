import React, { memo } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

const styles  = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: "#000",
    },
    image: {
        flex: 1,
        marginStart: 20
    },
    descriptionView: {
        flex: 2,
        marginStart: 10,
    },
    playlistTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: "bold"
    },
    playlistOwner: {
        color: '#aaa',
    },
    playlistDescription: {
        color: '#fff',
        marginTop: 15,
        marginBottom: 5
    },
    playlistFollowers: {
        color: '#aaa',

    }
})

function simplifyTotal(total) {
    if (total >= 1000000) {
        return (total / 1000000).toPrecision(3) + "M"
    }
    if (total >= 10000) {
        return (total / 10000).toPrecision(3) + "K"
    }
    else return total
}

export default memo(({ playlist }) => {
    const total = simplifyTotal(playlist.followers.total)
    return (
        <View style={styles.container}>
            <View style={styles.image}>
                <Image 
                source={{uri: playlist.images[0].url}}
                style={{height: 125, width:125}} />
            </View>
            <View style={styles.descriptionView}>
                <Text style={styles.playlistTitle}>{playlist.name}</Text>
                <Text style={styles.playlistOwner}>Playlist by {playlist.owner.display_name}</Text>
                <Text style={styles.playlistDescription}>{playlist.description}</Text>
                <Text style={styles.playlistFollowers}>{total} followers</Text>
            </View>
        </View>
    )
})
