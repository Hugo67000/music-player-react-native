import React, { memo, useEffect, useState } from 'react'
import SongItem from './SongItem'
import { Audio } from 'expo-av'
import { StyleSheet, View, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const styles  = StyleSheet.create({
    container: {
        backgroundColor: '#000',
        marginBottom: 20,
        flexDirection: 'row',
        display: "flex"
    },
    player: {
        alignSelf: "flex-end",
        alignItems: "flex-end",
        alignContent: "flex-end",
        marginTop: 15,
        marginStart: 5
    },
    img: {
        width: 50,
        height: 50,
    }
})

export default memo(({track}) => {
    const [playing, setPlaying] = useState(false)
    const [musicToPlay, setMusicToPlay] = useState(null)
    const pausePicutre = require("../assets/pause.png")
    const playPicture = require("../assets/play.png")
    const manageSong = async () => {
        if (playing) {
            await musicToPlay.playAsync()
        } else {
            await musicToPlay.pauseAsync()
        }
        setPlaying(!playing)
    }
    useEffect(() => {
        if (musicToPlay == null) {
            (async () => {
                const { sound } = await Audio.Sound.createAsync({
                    uri : track.preview_url
                })
                setMusicToPlay(sound)
            })()
        }  
    })

    return (
        <TouchableOpacity style={styles.container} onPress={()=> manageSong()}>
            <SongItem style={{flex: 8}} track={track} />
            <View style={styles.player}>
            <Image
                source={playing ? playPicture : pausePicutre }
                style={styles.img}
                />

            </View>
        </TouchableOpacity>
        
    )

})