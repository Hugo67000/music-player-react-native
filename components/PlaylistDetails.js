import React, { memo, useState, useEffect } from 'react'
import PlaylistDescription from './PlaylistDescription';
import SongItem from './SongItem';
import Player from './Player';
import SplashScreen from './SplashScreen';
import { StyleSheet, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { getPlaylistDetails } from '../data/playlists'


const styles  = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
        paddingTop: 40,
        height: "100%",
        width: "100%",
    },
    secondContainer: {
        flex: 8,
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    text: {
        color: '#fff'
    },
    description: {
        flex: 2,
    },
    trackList: {
        flex: 5,
        margin: 5
    },
    player: {
        flex: 1
    }
})

const playMusic = () => {console.log("playing")}

export default memo(({ route }) => {

    const [playlist, setPlaylist] = useState(null)
    const [playing, setPlaying] = useState(null)
    useEffect(() => {
        if (playlist == null) {
            (async () => {
                const data = await getPlaylistDetails(route.params);
                setPlaylist(data);
            })()
        }  
    })

    if (playlist == null) return (<SplashScreen />)
    return (
        <View style={styles.container}>
             <View style={styles.secondContainer}>
                <PlaylistDescription playlist={playlist} style={styles.description}/>       
                <FlatList
                    data={playlist.tracks.items}
                    keyExtractor={(item) => item.track.id.toString()}
                    renderItem={({item}) => <SongItem track={item.track} onPress={() => {
                        playMusic(item.track)
                        setPlaying(item.track)
                    }} /> }
                    style={styles.trackList}
                />
            </View>
            { playing &&
                <Player track={playing} style={styles.player} />
            }
        </View>
    )
})