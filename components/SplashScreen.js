import { SplashScreen } from "expo";
import React, { memo } from "react";
import { View, StyleSheet, Image } from 'react-native'
import loading from '../assets/loading.gif'


const styles = StyleSheet.create({
    container: {
        height: "100%",
        width: "100%",
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black'
    },
    textStyles: {
        color: 'white',
        fontSize: 40,
        fontWeight: 'bold'
    }
});

export default memo(() => {
    return (
    <View style={styles.container}>
        <Image source={loading} />
    </View>
    );
})