import React, { memo } from 'react'
import { StyleSheet, View, Image, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const imageWidth = Dimensions.get('window').width / 2 - 10
const styles  = StyleSheet.create({
    image: {
        width : imageWidth,
        height : imageWidth
    },
    container: {
        margin : 5
    }
})

export default memo(({ onPress, playlist }) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPress}>
            <Image
                source={{uri: playlist.images[0].url}}
                style={styles.image}
            />
            </TouchableOpacity>
        </View>
    )
})