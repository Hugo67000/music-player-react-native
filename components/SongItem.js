import React, { memo } from 'react'
import { StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const styles  = StyleSheet.create({
    container: {
        backgroundColor: '#000',
        marginLeft: 20,
        marginTop: 20
    },
    artistText: {
        color: '#fff',
        fontWeight: "bold"
    },
    trackText: {
        color: '#aaa'
    },
    artistGrey: {
        color: "#888",
        fontWeight: "bold"
    },
    trackGrey: {
        color: "#888"
    }
})

export default memo(({ track, onPress }) => {
    const artists = []
    track.artists.forEach(artist => artists.push(artist.name));
    if (track.preview_url) {
        return (
            <TouchableOpacity style={styles.container} onPress={onPress}>
                <Text style={styles.artistText}>{artists.join(", ")}</Text>
                <Text style={styles.trackText}>{track.name}</Text>
            </TouchableOpacity>
        )
    }
    else {
        return (
            <View style={styles.container}>
                <Text style={styles.artistGrey}>{artists.join(", ")}</Text>
                <Text style={styles.trackGrey}>{track.name}</Text>
            </View>
        )
    }
})