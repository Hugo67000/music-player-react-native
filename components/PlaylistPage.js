import React, { useState, useEffect, memo } from 'react'
import PlaylistItem from './PlaylistItem'
import { View, StyleSheet } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import { getPlaylists } from '../data/playlists'
import SplashScreen from './SplashScreen'


const styles = StyleSheet.create({
    container: {
        height: "100%",
        width: "100%",
        backgroundColor: '#000',
      },
    topView: {
        position: "absolute"
    }
});

export default memo(({ navigation }) => {
    const [playlists, setPlaylists] = useState([])
    useEffect(() => {
        (async function getPlaylistsContent() {
            const data = await getPlaylists()
            let plays = []
            data.playlists.items.forEach(item => {
                plays.push(item)
            })
            setPlaylists(plays)
        })()
    })
    return (
        <View style={styles.container}>
            <FlatList
                data={playlists}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({item}) =>
                    <PlaylistItem playlist={item} onPress={() => navigation.navigate('PlaylistDetails', item.id)}/>
                }
                numColumns={2}
            />
        </View>
    )
})