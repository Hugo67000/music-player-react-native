const endpoint = "https://afternoon-waters-49321.herokuapp.com"
const playlist = "/v1/browse/featured-playlists"
const playlistEndpoint = endpoint + playlist
const playlistDetails = "/v1/playlists/"
const playlistDetailsEndpoint = endpoint + playlistDetails

export let cachedPlaylists = {}

export const getPlaylists = async () => {
  try {
    const response = await fetch(playlistEndpoint)
    return await response.json()    
  }
  catch (error) {
    console.error(error)
  }
}

export const getPlaylistDetails = async (id) => {
  if (cachedPlaylists[id]) {
    return cachedPlaylists[id]
  }
  try {
    const response = await fetch(playlistDetailsEndpoint + id)
    const playlist = await response.json()
    cachedPlaylists[id] = playlist
    return playlist
  }
  catch (error) {
    console.error(error)
  }
}

export let PlaylistData = class PlaylistData {
    constructor(object) {
        this.external_urls = object.external_urls
        this.href = object.href
        this.id = object.id
        this.images = object.images
        this.name = object.name
        this.owner = object.owner
        this.ownerName = this.owner.display_name
        this.primary_color = object.primary_color
        this.public = object.public
        this.snapshot_id = object.snapshot_id
        this.tracks = object.tracks
        this.type = object.type
        this.uri = object.uri
    }
}

